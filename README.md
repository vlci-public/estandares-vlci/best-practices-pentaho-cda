**Índice**  

 [[_TOC_]]  
  

# Pentaho CDA. Conceptos Fundamentales.

Aquí se listan todas las buenas prácticas a la hora de realizar un CDA en el Pentaho desligado del LDAP del Ayuntamiento cuya URL es [Pentaho CDAs](https://datosbi.vlci.valencia.es/pentaho/Home)
Pero antes de listar las buenas prácticas, se explicarán ciertos conceptos fundamentales


## Estructura de carpetas global.

En el Pentaho de CDAs, en la carpeta PUBLIC, se tienen las siguientes carpetas usadas por varios proyectos:
- `vlci`: Usada por parte de la OTP, dentro del proyecto VLCi. Contendrá todos los CDAs en el entorno productivo
- `vlci_pre`: Usada por parte de la OTP, dentro del proyecto VLCi. Contendrá todos los CDAs en un entorno de pruebas
- `edificios`: Usada por parte del proyecto de  Edificios Inteligentes, gestionado por Telefónica.
- `urbo`: Usada por Telefónica, dentro del proyecto VLCi, para los cuadros de mandos de Urbo


## Estructura de carpetas proyecto VLCi.

Dentro de las carpetas del proyecto VLCi (PUBLIC/vlci y PUBLIC/vlci_pre), actualmente se tienen 3 grandes proyectos donde ubicar CDAs:
- `datosabiertos`: Ubicación de CDAs para publicar en el portal de datos abiertos del Ayuntamiento de Valencia.
- `datos3os`: Ubicación de CDAs para distribuir a desarrolladores externos o empresas externas que necesiten consumir datos de la plataforma VLCi.
- `vminut`: CDAs usados por la aplicación web del Servicio de Ciudad Inteligente, desarrollado por el equipo de la OTP del proyecto VLCi: Valenciaalminut.

Existen 4 usuarios de aplicación con permisos a cada uno de los grandes proyectos:
- `publicoda`: con acceso de lectura al proyecto datosabiertos. La contraseña es pública. Cualquier persona con el endpoint del CDA tendrá por tanto acceso sin restricciones.
- `publicod3`: con acceso de lectura al proyecto datos3os. La contraseña es pública. Cualquier persona con el endpoint del CDA tendrá por tanto acceso sin restricciones.
- `vminutprivado`: con acceso de lectura a la carpeta vlci/vminut. La contraseña es privada. Sólo la usa el backend de Valenciaalminut para lanzar peticiones HTTP al CDA.
- `vminutpreprivado`: Igual que el usuario anterior pero con acceso de lectura a la carpeta vlci_pre/vminut.


# CDAs desde colecciones Mongo.

En general los CDAs leerán los datos desde la base de datos de plataforma Postgis, pero puede darse el caso de que el dato no se encuentre en Postgis. En ese caso se obtendrá el dato para el CDA desde el Mongo de plataforma.
Existe un HowTo que explica cómo crear a través del Pentaho Data Integration (PDI) un fichero kettle (con extensión .ktr).

## Nombrado de los Kettles que proporcionan datos desde Mongo a los CDAs.

Se seguirá la misma norma de nombrado que para los CDAs que sirven.

## Ubicación de los Kettles que proporcionan datos desde Mongo a los CDAs.

Los ficheros PDI (extensión .ktr) se ubicarán en la misma carpeta de los CDAs que los usan. Cabe destacar que estos archivos no serán visibles directamente en la interfaz web de Pentaho. Para acceder a ellos, habrá que descargar la carpeta en la que estén ubicados.

# Proyecto VLCi, OTP. Pentaho CDA. Buenas Prácticas.

## Nombrado de los CDAs.

### CDAs para Datos Abiertos.
Todos los CDAs ubicados en PUBLIC/vlci/datosabiertos (y en su misma ruta, pero de PRE) seguirán la siguiente norma de nombrado:

\[SERVICIO_RESPONSABLE_CDA]_\[NOMBRE_DESCRIPTIVO]

Ejemplos de servicio responsable:
`ciudadinteligente`, `movilidad`, `calidadambiental`, `tributaria`, etc

Ejemplos de nombre completo de CDA:
`calidadambiental_sonometros_zas_semanales.cda`

Para construir CDAs sin contraseña, ver el capítulo: [CDAs abiertos o sin contraseña](#cdas-abiertos-o-sin-contraseña)


### CDAs para Integradores Externos o Entidades Externas.
Todos los CDAs ubicados en PUBLIC/vlci/datos3os (y en su misma ruta, pero de PRE) seguirán la siguiente norma de nombrado:

\[DESTINATARIO]\_\[PROYECTO]\_\[NOMBRE_DESCRIPTIVO]

Ejemplos de nombre completo de CDA:
`upv_citcom_aire_contaminantes60min.cda`

Para construir CDAs sin contraseña, ver el capítulo: [CDAs abiertos o sin contraseña](#cdas-abiertos-o-sin-contraseña)


### CDAs para proyectos Internos, ejemplo Valenciaalminut.
Todos los CDAs ubicados en PUBLIC/vlci/vminut (y en su misma ruta, pero de PRE) seguirán la siguiente norma de nombrado:

\[WIDGET_DONDE_SE_USA]_\[NOMBRE_DESCRIPTIVO]

Widgets de Valenciaalminut:
- `cuadros_ciudad`
- `info_ciclista_valenbisi`
- `aparcamientos_publicos`
- `aparcamientos_smart`
- `tiempo`
- `calidad_aire`
- `ocupacion`

Ejemplos de nombre completo de CDA:
`calidad_aire_contaminantes60min.cda`


## Ficheros en el servidor.

Para crear un CDA se empezará creando un nuevo dashboard. Pero una vez creado el CDA borraremos los ficheros innecesarios, que son los que tienen la extensión: .wcdf y .cdfde, para no tener ficheros innecesarios en el servidor.


## CDAs abiertos o sin contraseña.

Se ha realizado una nueva implementación que permite construir la URL del CDA público sin la necesidad de establecer la contraseña en la misma. Este mecanismo se usará para:
 - CDAs para Datos Abiertos.
 - CDAs para Integradores Externos o Entidades Externas.


La forma de construirla es la siguiente:
* Se sustituye el parámetro userid=xxxx por _TRUST_USER_=xxxx
* Se elimina el parámetro password=xxxx.

Una URL de ejemplo sería:
https://datosbi.vlci.valencia.es/pentaho/plugin/cda/api/doQuery?path=/public/vlci_pre/datosabiertos/movilidad_impulso_vehiculo_electrico.cda&dataAccessId=kpi_vehiculos_electricos&paramkpi=IS_SCT_230301&_TRUST_USER_=publicoda&outputType=csv
 
Esta nueva funcionalidad únicamente se aplica cuando:
* El nombre del usuario empieza con el literal “publico”
* La URL es una de las siguientes: /pentaho/plugin/cda/api/doQuery o /pentaho/plugin/cda/api/previewQuery


# URBO. Pentaho CDA. Buenas Prácticas.

*PENDIENTE*

# Edificios Inteligentes. Pentaho CDA. Buenas Prácticas.

*PENDIENTE*
